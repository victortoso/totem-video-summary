/*
 * Copyright (C) 2015 Victor Toso.
 *
 * Contact: Victor Toso <me@victortoso.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "totem-video-summary.h"

#include <net/grl-net.h>
#include <string.h>

typedef struct _TotemVideosSummaryPrivate
{
  GrlRegistry *registry;
  GrlSource *tmdb_source;
  GrlSource *tvdb_source;
  GrlSource *video_title_parsing_source;
  GrlSource *opensubtitles_source;
  GrlKeyID tvdb_poster_key;
  GrlKeyID tmdb_poster_key;
  GrlKeyID subtitles_lang_key;
  GrlKeyID subtitles_url_key;

  GtkImage *poster;
  GtkLabel *title;
  GtkLabel *summary;
  GtkLabel *released;
  GtkLabel *genre;
  GtkLabel *cast;
  GtkLabel *directors;
  GtkLabel *authors;

  GtkComboBoxText   *subtitles_combo;

  GtkButton *arrow_right;
  GtkButton *arrow_left;

  GList *current_video;
  GList *videos;

  gboolean movie_enabled;
  gboolean shows_enabled;
  gboolean subtitles_enabled;

  GList *pending_ops;
} TotemVideosSummaryPrivate;

typedef struct _VideoSummaryData VideoSummaryData;

typedef struct
{
  TotemVideosSummary *totem_videos_summary;
  GrlMedia           *video;

  gchar    *poster_path;
  gboolean  is_tv_show;

  VideoSummaryData *video_summary;
  GList            *pending_grl_ops;
} OperationSpec;

struct _VideoSummaryData
{
  gchar    *title;
  gchar    *description;
  gchar    *genre;
  gchar    *performer;
  gchar    *director;
  gchar    *author;
  gchar    *poster_path;
  gchar    *publication_date;
  gboolean  is_tv_show;

  GHashTable *subtitles;
};

#define POSTER_WIDTH  266
#define POSTER_HEIGHT 333

/* FIXME: Almost random. Probably we don't want to use wrap-width :) */
#define WRAP_WIDTH_SUBTITLES(n) ((n > 25) ? 8 : 4)

static gchar *get_data_from_media (GrlData *data, GrlKeyID key);

G_DEFINE_TYPE_WITH_PRIVATE (TotemVideosSummary, totem_videos_summary, GTK_TYPE_GRID);

/* -------------------------------------------------------------------------- *
 * Internal / Helpers
 * -------------------------------------------------------------------------- */

static void
operation_spec_free (OperationSpec *os)
{
  TotemVideosSummaryPrivate *priv;

  if (os->pending_grl_ops != NULL) {
    /* Wait pending grilo operations to finish */
    return;
  }

  priv = os->totem_videos_summary->priv;
  priv->pending_ops = g_list_remove (priv->pending_ops, os);

  g_clear_object (&os->video);
  g_clear_pointer (&os->poster_path, g_free);
  g_slice_free (OperationSpec, os);
}

static void
video_summary_data_free (gpointer gp)
{
  VideoSummaryData *data = gp;

  g_free (data->title);
  g_free (data->description);
  g_free (data->genre);
  g_free (data->performer);
  g_free (data->director);
  g_free (data->author);
  g_free (data->poster_path);
  g_free (data->publication_date);
  g_slice_free (VideoSummaryData, data);
}

static void
totem_videos_summary_set_data_content (TotemVideosSummary *self,
                                       VideoSummaryData   *data)
{
  TotemVideosSummaryPrivate *priv = self->priv;

  if (data->title)
    gtk_label_set_text (priv->title, data->title);

  if (data->description) {
    gtk_label_set_text (priv->summary, data->description);
    gtk_widget_set_visible (GTK_WIDGET(priv->summary), TRUE);
  } else {
    gtk_widget_set_visible (GTK_WIDGET(priv->summary), FALSE);
  }

  if (data->genre) {
    gtk_label_set_text (priv->genre, data->genre);
    gtk_widget_set_visible (GTK_WIDGET(priv->genre), TRUE);
  } else {
    gtk_widget_set_visible (GTK_WIDGET(priv->genre), FALSE);
  }

  if (data->performer) {
    gtk_label_set_text (priv->cast, data->performer);
    gtk_widget_set_visible (GTK_WIDGET(priv->cast), TRUE);
  } else {
    gtk_widget_set_visible (GTK_WIDGET(priv->cast), FALSE);
  }

  if (data->director) {
    gtk_label_set_text (priv->directors, data->director);
    gtk_widget_set_visible (GTK_WIDGET(priv->directors), TRUE);
  } else {
    gtk_widget_set_visible (GTK_WIDGET(priv->directors), FALSE);
  }

  if (data->author) {
    gtk_label_set_text (priv->authors, data->author);
    gtk_widget_set_visible (GTK_WIDGET(priv->authors), TRUE);
  } else {
    gtk_widget_set_visible (GTK_WIDGET(priv->authors), FALSE);
  }

  if (data->poster_path) {
    GError *error = NULL;
    GdkPixbuf *pixbuf;

    pixbuf = gdk_pixbuf_new_from_file_at_scale (data->poster_path,
                                                POSTER_WIDTH,
                                                POSTER_HEIGHT,
                                                TRUE,
                                                &error);
    if (error != NULL) {
      g_warning ("no poster due: %s", error->message);
      g_error_free (error);
      gtk_image_clear (priv->poster);
    } else {
      gtk_image_set_from_pixbuf (priv->poster, pixbuf);
      g_object_unref (pixbuf);
    }
  } else {
    /* FIXME: Maybe a "No poster" image can be displayed instead */
    gtk_image_clear (priv->poster);
  }

  if (data->publication_date) {
    gtk_label_set_text (priv->released, data->publication_date);
    gtk_widget_set_visible (GTK_WIDGET(priv->released), TRUE);
  } else {
    gtk_widget_set_visible (GTK_WIDGET(priv->released), FALSE);
  }

  if (data->subtitles) {
    GHashTableIter iter;
    gpointer key;

    gtk_combo_box_text_remove_all (priv->subtitles_combo);
    g_hash_table_iter_init (&iter, data->subtitles);
    while (g_hash_table_iter_next (&iter, &key, NULL)) {
      const char *lang = key;
      gtk_combo_box_text_append (priv->subtitles_combo, lang, lang);
    }
    gtk_combo_box_set_wrap_width (GTK_COMBO_BOX (priv->subtitles_combo),
                                  WRAP_WIDTH_SUBTITLES (g_hash_table_size (data->subtitles)));
    gtk_combo_box_set_active (GTK_COMBO_BOX (priv->subtitles_combo), 0);
    gtk_widget_set_visible (GTK_WIDGET(priv->subtitles_combo), TRUE);
  } else {
    gtk_widget_set_visible (GTK_WIDGET(priv->subtitles_combo), FALSE);
  }
}

static void
totem_videos_summary_set_basic_content (TotemVideosSummary *self,
                                        GrlMedia           *video)
{
  const gchar *title;
  gboolean is_tv_show = (grl_media_get_show (video) != NULL);

  if (is_tv_show)
    title = grl_media_get_episode_title (video);
  else
    title = grl_media_get_title (video);

  if (title)
    gtk_label_set_text (self->priv->title, title);
}

static void
totem_videos_summary_update_ui (TotemVideosSummary *self)
{
  VideoSummaryData *data;
  gboolean visible;
  TotemVideosSummaryPrivate *priv = self->priv;

  if (priv->videos == NULL) {
    g_warning ("Don't have any video to display");
    return;
  } else if (priv->current_video == NULL) {
    priv->current_video = priv->videos;
  }

  data = priv->current_video->data;
  totem_videos_summary_set_data_content (self, data);

  /* For optimization, list is inverted */
  visible = (priv->current_video->next != NULL);
  gtk_widget_set_visible (GTK_WIDGET(priv->arrow_left), visible);

  visible = (priv->current_video->prev != NULL);
  gtk_widget_set_visible (GTK_WIDGET(priv->arrow_right), visible);
}

static void
change_video_cb (GtkWidget *button,
                 gpointer   user_data)
{
  TotemVideosSummaryPrivate *priv;
  const gchar *name;
  GList *next_video = NULL;

  priv = TOTEM_VIDEOS_SUMMARY (user_data)->priv;
  name = gtk_widget_get_name (button);

  /* List is inverted */
  if (g_str_equal (name, "forward-arrow")) {
    next_video = g_list_previous (priv->current_video);
    if (!next_video)
      next_video = g_list_last (priv->videos);
  } else {
    next_video = g_list_next (priv->current_video);
    if (!next_video)
      next_video = priv->videos;
  }

  priv->current_video = next_video;
  totem_videos_summary_update_ui (user_data);
}

static void
video_summary_set_subtitles (TotemVideosSummary *self,
                             VideoSummaryData   *video_summary,
                             GrlData            *data)
{
  guint i, length;
  TotemVideosSummaryPrivate *priv;

  if (video_summary == NULL || video_summary->subtitles != NULL)
    return;

  priv = self->priv;
  length = grl_data_length (data, priv->subtitles_lang_key);

  if (length == 0)
    return;

  video_summary->subtitles = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
  for (i = 0; i < length; i++) {
    GrlRelatedKeys *relkeys;
    const gchar *sub_url, *sub_lang;

    relkeys = grl_data_get_related_keys (data, priv->subtitles_lang_key, i);
    sub_lang = grl_related_keys_get_string (relkeys, priv->subtitles_lang_key);
    sub_url = grl_related_keys_get_string (relkeys, priv->subtitles_url_key);
    if (sub_lang == NULL || sub_url == NULL)
      continue;

    g_hash_table_insert (video_summary->subtitles,
                         g_strdup (sub_lang),
                         g_strdup (sub_url));
  }
}

static void
add_video_to_summary_and_free (OperationSpec *os)
{
  TotemVideosSummary *self = os->totem_videos_summary;
  VideoSummaryData *data;
  GDateTime *released;

  data = g_slice_new0 (VideoSummaryData);
  data->is_tv_show = (grl_media_get_show (os->video) != NULL);
  data->description = g_strdup (grl_media_get_description (os->video));
  data->genre = get_data_from_media (GRL_DATA (os->video), GRL_METADATA_KEY_GENRE);
  data->performer = get_data_from_media (GRL_DATA (os->video),
                                         GRL_METADATA_KEY_PERFORMER);
  data->director = get_data_from_media (GRL_DATA (os->video),
                                        GRL_METADATA_KEY_DIRECTOR);
  data->author = get_data_from_media (GRL_DATA (os->video),
                                      GRL_METADATA_KEY_AUTHOR);
  data->poster_path = g_strdup (os->poster_path);

  released = grl_media_get_publication_date (os->video);
  if (released)
    data->publication_date = g_date_time_format (released, "%F");

  if (data->is_tv_show)
    data->title = g_strdup (grl_media_get_episode_title (os->video));
  else
    data->title = g_strdup (grl_media_get_title (os->video));

  video_summary_set_subtitles (self, os->video_summary, GRL_DATA (os->video));

  /* Cache VideoSummaryData as we might have other async calls */
  os->video_summary = data;

  self->priv->videos = g_list_prepend (self->priv->videos, data);
  totem_videos_summary_update_ui (self);
  operation_spec_free (os);
}

static void
resolve_poster_done (GObject      *source_object,
                     GAsyncResult *res,
                     gpointer      user_data)
{
  OperationSpec *os;
  gchar *data;
  gsize len;
  GError *err = NULL;

  os = user_data;
  grl_net_wc_request_finish (GRL_NET_WC (source_object),
                             res, &data, &len, &err);
  if (err != NULL) {
    g_warning ("Fetch image failed due: %s", err->message);
    g_error_free (err);
  } else {
    g_file_set_contents (os->poster_path, data, len, &err);
  }

  /* Update interface */
  add_video_to_summary_and_free (os);
}

static void
resolve_secondary_metadata_done (GrlSource    *source,
                                 guint         operation_id,
                                 GrlMedia     *media,
                                 gpointer      user_data,
                                 const GError *error)
{
  TotemVideosSummary *self;
  OperationSpec *os = user_data;

  os->pending_grl_ops = g_list_remove (os->pending_grl_ops,
                                       GUINT_TO_POINTER (operation_id));
  if (error) {
    g_warning ("Resolve operation failed: %s", error->message);
    operation_spec_free (os);
    return;
  }

  self = os->totem_videos_summary;

  if (os->video_summary != NULL) {
    /* Primary metadata was already set */
    video_summary_set_subtitles (self, os->video_summary, GRL_DATA (media));
    totem_videos_summary_update_ui (self);
    return;
  }

  add_video_to_summary_and_free (os);
}

static void
resolve_metadata_done (GrlSource    *source,
                       guint         operation_id,
                       GrlMedia     *media,
                       gpointer      user_data,
                       const GError *error)
{
  TotemVideosSummaryPrivate *priv;
  OperationSpec *os = user_data;
  const gchar *title, *poster_url;

  os->pending_grl_ops = g_list_remove (os->pending_grl_ops,
                                       GUINT_TO_POINTER (operation_id));

  if (error) {
    g_warning ("Resolve operation failed: %s", error->message);
    operation_spec_free (os);
    return;
  }

  priv = os->totem_videos_summary->priv;

  if (os->is_tv_show)
    title = grl_media_get_show (media);
  else
    title = grl_media_get_title (media);

  if (title == NULL) {
    g_warning ("Basic information is missing - no title");
    operation_spec_free (os);
    return;
  }

  if (os->is_tv_show)
    poster_url = grl_data_get_string (GRL_DATA (media), priv->tvdb_poster_key);
  else
    poster_url = grl_data_get_string (GRL_DATA (media), priv->tmdb_poster_key);

  if (poster_url != NULL) {
    os->poster_path = g_build_filename (g_get_tmp_dir (), title, NULL);
    if (!g_file_test (os->poster_path, G_FILE_TEST_EXISTS)) {
      GrlNetWc *wc = grl_net_wc_new ();
      grl_net_wc_request_async (wc, poster_url, NULL, resolve_poster_done, os);
      g_object_unref (wc);
      return;
    }
  }

  add_video_to_summary_and_free (os);
}

static void
resolve_by_tmdb (OperationSpec *os)
{
  TotemVideosSummaryPrivate *priv;
  GrlOperationOptions *options;
  GList *keys;
  GrlCaps *caps;
  guint op_id;

  priv = os->totem_videos_summary->priv;

  caps = grl_source_get_caps (priv->tmdb_source, GRL_OP_RESOLVE);
  options = grl_operation_options_new (caps);
  grl_operation_options_set_resolution_flags (options, GRL_RESOLVE_NORMAL);

  keys = grl_metadata_key_list_new (GRL_METADATA_KEY_DESCRIPTION,
                                    GRL_METADATA_KEY_PERFORMER,
                                    GRL_METADATA_KEY_DIRECTOR,
                                    GRL_METADATA_KEY_AUTHOR,
                                    GRL_METADATA_KEY_GENRE,
                                    GRL_METADATA_KEY_PUBLICATION_DATE,
                                    priv->tmdb_poster_key,
                                    GRL_METADATA_KEY_INVALID);
  op_id = grl_source_resolve (priv->tmdb_source,
                              os->video,
                              keys,
                              options,
                              resolve_metadata_done,
                              os);
  g_object_unref (options);
  g_list_free (keys);

  os->pending_grl_ops = g_list_prepend (os->pending_grl_ops,
                                        GUINT_TO_POINTER (op_id));
}

static void
resolve_by_the_tvdb (OperationSpec *os)
{
  TotemVideosSummaryPrivate *priv;
  GrlOperationOptions *options;
  GList *keys;
  GrlCaps *caps;
  guint op_id;

  priv = os->totem_videos_summary->priv;
  caps = grl_source_get_caps (priv->tvdb_source, GRL_OP_RESOLVE);
  options = grl_operation_options_new (caps);
  grl_operation_options_set_resolution_flags (options, GRL_RESOLVE_NORMAL);

  keys = grl_metadata_key_list_new (GRL_METADATA_KEY_DESCRIPTION,
                                    GRL_METADATA_KEY_PERFORMER,
                                    GRL_METADATA_KEY_DIRECTOR,
                                    GRL_METADATA_KEY_AUTHOR,
                                    GRL_METADATA_KEY_GENRE,
                                    GRL_METADATA_KEY_PUBLICATION_DATE,
                                    GRL_METADATA_KEY_EPISODE_TITLE,
                                    priv->tvdb_poster_key,
                                    GRL_METADATA_KEY_INVALID);
  op_id = grl_source_resolve (priv->tvdb_source,
                              os->video,
                              keys,
                              options,
                              resolve_metadata_done,
                              os);
  g_object_unref (options);
  g_list_free (keys);

  os->pending_grl_ops = g_list_prepend (os->pending_grl_ops,
                                        GUINT_TO_POINTER (op_id));
}

static void
resolve_by_opensubtitles (OperationSpec *os)
{
  TotemVideosSummaryPrivate *priv;
  GrlOperationOptions *options;
  GList *keys;
  GrlCaps *caps;
  guint op_id;

  priv = os->totem_videos_summary->priv;
  caps = grl_source_get_caps (priv->opensubtitles_source, GRL_OP_RESOLVE);
  options = grl_operation_options_new (caps);
  grl_operation_options_set_resolution_flags (options, GRL_RESOLVE_NORMAL);

  keys = grl_metadata_key_list_new (priv->subtitles_lang_key,
                                    priv->subtitles_url_key,
                                    GRL_METADATA_KEY_INVALID);
  op_id = grl_source_resolve (priv->opensubtitles_source,
                              os->video,
                              keys,
                              options,
                              resolve_secondary_metadata_done,
                              os);
  g_object_unref (options);
  g_list_free (keys);

  os->pending_grl_ops = g_list_prepend (os->pending_grl_ops,
                                        GUINT_TO_POINTER (op_id));
}

static void
resolve_video_summary_media (OperationSpec *os)
{
  TotemVideosSummaryPrivate *priv;
  priv = os->totem_videos_summary->priv;

  if (priv->shows_enabled &&
      grl_media_get_show (os->video) != NULL) {
    os->is_tv_show = TRUE;
    totem_videos_summary_set_basic_content (os->totem_videos_summary, os->video);
    resolve_by_the_tvdb (os);

    if (priv->subtitles_enabled)
      resolve_by_opensubtitles (os);
    return;
  }

  if (priv->movie_enabled &&
      grl_media_get_title (os->video) != NULL) {
    os->is_tv_show = FALSE;
    totem_videos_summary_set_basic_content (os->totem_videos_summary, os->video);
    resolve_by_tmdb (os);

    if (priv->subtitles_enabled)
      resolve_by_opensubtitles (os);
    return;
  }

  g_warning ("video type is not defined: %s", grl_media_get_url (os->video));
  operation_spec_free (os);
}

static void
resolve_by_video_title_parsing_done (GrlSource    *source,
                                     guint         operation_id,
                                     GrlMedia     *media,
                                     gpointer      user_data,
                                     const GError *error)
{
  OperationSpec *os = user_data;

  os->pending_grl_ops = g_list_remove (os->pending_grl_ops,
                                       GUINT_TO_POINTER (operation_id));
  if (error != NULL) {
    g_warning ("video-title-parsing failed: %s", error->message);
    operation_spec_free (os);
    return;
  }

  resolve_video_summary_media (os);
}

static void
resolve_by_video_title_parsing (OperationSpec *os)
{
  TotemVideosSummaryPrivate *priv;
  GrlOperationOptions *options;
  GList *keys;
  GrlCaps *caps;
  guint op_id;

  priv = os->totem_videos_summary->priv;
  caps = grl_source_get_caps (priv->video_title_parsing_source, GRL_OP_RESOLVE);
  options = grl_operation_options_new (caps);
  grl_operation_options_set_resolution_flags (options, GRL_RESOLVE_NORMAL);

  keys = grl_metadata_key_list_new (GRL_METADATA_KEY_TITLE,
                                    GRL_METADATA_KEY_EPISODE_TITLE,
                                    GRL_METADATA_KEY_SHOW,
                                    GRL_METADATA_KEY_SEASON,
                                    GRL_METADATA_KEY_EPISODE,
                                    GRL_METADATA_KEY_INVALID);

  /* We want to extract all metadata from file's name */
  grl_data_set_boolean (GRL_DATA (os->video),
                        GRL_METADATA_KEY_TITLE_FROM_FILENAME,
                        TRUE);
  op_id = grl_source_resolve (priv->video_title_parsing_source,
                              os->video,
                              keys,
                              options,
                              resolve_by_video_title_parsing_done,
                              os);
  g_object_unref (options);
  g_list_free (keys);

  os->pending_grl_ops = g_list_prepend (os->pending_grl_ops,
                                        GUINT_TO_POINTER (op_id));
}

/* For GrlKeys that have several values, return all of them in one
 * string separated by comma; */
static gchar *
get_data_from_media (GrlData *data,
                     GrlKeyID key)
{
  gint i, len;
  GString *s;

  len = grl_data_length (data, key);
  if (len <= 0)
    return NULL;

  s = g_string_new ("");
  for (i = 0; i < len; i++) {
    GrlRelatedKeys *relkeys;
    const gchar *element;

    relkeys = grl_data_get_related_keys (data, key, i);
    element = grl_related_keys_get_string (relkeys, key);

    if (i > 0)
      g_string_append (s, ", ");
    g_string_append (s, element);
  }
  return g_string_free (s, FALSE);
}

/* -------------------------------------------------------------------------- *
 * External
 * -------------------------------------------------------------------------- */

TotemVideosSummary *
totem_videos_summary_new (void)
{
  TotemVideosSummary *self;
  TotemVideosSummaryPrivate *priv;
  GrlSource *source;
  GrlRegistry *registry;

  self = g_object_new (TOTEM_TYPE_VIDEOS_SUMMARY, NULL);
  priv = self->priv;
  registry = grl_registry_get_default();
  priv->registry = registry;

  /* Those plugins should be loaded as requirement */
  source = grl_registry_lookup_source (registry, "grl-tmdb");
  if (source != NULL) {
    priv->tmdb_source = source;
    priv->tmdb_poster_key =
        grl_registry_lookup_metadata_key (registry, "tmdb-poster");
    priv->movie_enabled = TRUE;
  } else {
    priv->movie_enabled = FALSE;
    g_warning ("Failed to load tmdb source");
  }

  source = grl_registry_lookup_source (registry, "grl-thetvdb");
  if (source != NULL) {
    priv->tvdb_source = source;
    priv->tvdb_poster_key =
        grl_registry_lookup_metadata_key (registry, "thetvdb-poster");
    priv->shows_enabled = TRUE;
  } else {
    priv->shows_enabled = FALSE;
    g_warning ("Failed to load tvdb source");
  }

  if (!priv->movie_enabled && !priv->shows_enabled) {
    g_warning ("Base sources failed to load");
    g_object_unref (self);
    return NULL;
  }

  source = grl_registry_lookup_source (registry, "grl-video-title-parsing");
  g_return_val_if_fail (source != NULL, NULL);
  priv->video_title_parsing_source = source;

  source = grl_registry_lookup_source (registry, "grl-opensubtitles");
  if (source != NULL) {
    priv->opensubtitles_source = source;
    priv->subtitles_lang_key =
        grl_registry_lookup_metadata_key (registry, "subtitles-lang");
    priv->subtitles_url_key =
        grl_registry_lookup_metadata_key (registry, "subtitles-url");
    priv->subtitles_enabled = TRUE;
  } else {
    priv->subtitles_enabled = FALSE;
    g_warning ("Opensubtitles not available");
  }

  return self;
}

gboolean
totem_videos_summary_add_video (TotemVideosSummary *self,
                                GrlMedia           *video)
{
  const gchar *url;
  OperationSpec *os;

  g_return_val_if_fail (TOTEM_IS_VIDEOS_SUMMARY (self), FALSE);
  g_return_val_if_fail (video != NULL, FALSE);

  url = grl_media_get_url (video);
  if (url == NULL) {
    g_warning ("Video does not have url: can't initialize totem-video-summary");
    return FALSE;
  }

#ifndef ON_DEVELOPMENT
  if (!g_file_test (url, G_FILE_TEST_EXISTS)) {
    g_warning ("Video file does not exist");
    return FALSE;
  }
#endif

  os = g_slice_new0 (OperationSpec);
  os->totem_videos_summary = self;
  os->video = g_object_ref (video);
  self->priv->pending_ops = g_list_prepend (self->priv->pending_ops, os);
  if (self->priv->video_title_parsing_source != NULL) {
    resolve_by_video_title_parsing (os);
  } else {
    resolve_video_summary_media (os);
  }
  return TRUE;
}

/* -------------------------------------------------------------------------- *
 * Object
 * -------------------------------------------------------------------------- */

static void
totem_videos_summary_finalize (GObject *object)
{
  TotemVideosSummaryPrivate *priv = TOTEM_VIDEOS_SUMMARY (object)->priv;

  if (priv->videos) {
    g_list_free_full (priv->videos, video_summary_data_free);
    priv->videos = NULL;
  }

  if (priv->pending_ops) {
    GList *it = priv->pending_ops;

    /* Cancel all pending operations and release its data */
    while (it != NULL) {
      GList *next = it->next;
      OperationSpec *os = it->data;
      GList *it_os;

      for (it_os = os->pending_grl_ops; it_os != NULL; it_os = it_os->next) {
        grl_operation_cancel (GPOINTER_TO_UINT (it_os->data));
      }
      g_clear_pointer (&os->pending_grl_ops, g_list_free);
      operation_spec_free (os);
      it = next;
    }
    g_warn_if_fail (priv->pending_ops == NULL);
  }

  G_OBJECT_CLASS (totem_videos_summary_parent_class)->finalize (object);
}

static void
totem_videos_summary_init (TotemVideosSummary *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  self->priv = totem_videos_summary_get_instance_private (self);

  g_signal_connect (self->priv->arrow_right, "clicked", G_CALLBACK (change_video_cb), self);
  g_signal_connect (self->priv->arrow_left, "clicked", G_CALLBACK (change_video_cb), self);
}

static void
totem_videos_summary_class_init (TotemVideosSummaryClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

  object_class->finalize = totem_videos_summary_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/totem/grilo/totem-video-summary.ui");
  gtk_widget_class_bind_template_child_private (widget_class, TotemVideosSummary, poster);
  gtk_widget_class_bind_template_child_private (widget_class, TotemVideosSummary, title);
  gtk_widget_class_bind_template_child_private (widget_class, TotemVideosSummary, summary);
  gtk_widget_class_bind_template_child_private (widget_class, TotemVideosSummary, released);
  gtk_widget_class_bind_template_child_private (widget_class, TotemVideosSummary, subtitles_combo);
  gtk_widget_class_bind_template_child_private (widget_class, TotemVideosSummary, genre);
  gtk_widget_class_bind_template_child_private (widget_class, TotemVideosSummary, cast);
  gtk_widget_class_bind_template_child_private (widget_class, TotemVideosSummary, directors);
  gtk_widget_class_bind_template_child_private (widget_class, TotemVideosSummary, authors);
  gtk_widget_class_bind_template_child_private (widget_class, TotemVideosSummary, arrow_left);
  gtk_widget_class_bind_template_child_private (widget_class, TotemVideosSummary, arrow_right);
}
